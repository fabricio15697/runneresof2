package br.com.productregister;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
	
	List<Product> list;
	
	@Before
	public void init() {
		list = new ArrayList<Product>();
	}
	
	@Test
	public void testEmptyStack() {
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void testInsertProduct() {
		list.add(new Product("TV", 500.00));
		list.add(new Product("Car", 45500.00));
		list.add(new Product("Computer", 4500.00));
	}
	
	@Test
	public void testSizeList() {
		list.add(new Product("TV", 500.00));
		list.add(new Product("Car", 45500.00));
		list.add(new Product("Computer", 4500.00));
		int size = list.size();
		assertEquals(3, size);
	}
}
